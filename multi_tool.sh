#!/bin/bash
# Default variables
number=1
ram="512m"
uninstall="false"
# Options
. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/colors.sh)
option_value(){ echo "$1" | sed -e 's%^--[^=]*=%%g; s%^-[^=]*=%%g'; }
while test $# -gt 0; do
	case "$1" in
	-h|--help)
		. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/logo.sh)
		echo
		echo -e "${C_LGn}Functionality${RES}: the script installs or uninstalls several Minima nodes on a server"
		echo
		echo -e "${C_LGn}Usage${RES}: script ${C_LGn}[OPTIONS]${RES}"
		echo
		echo -e "${C_LGn}Options${RES}:"
		echo -e "  -h, --help           show help page"
		echo -e "  -n, --number NUMBER  NUMBER of nodes to be installed or uninstalled"
		echo -e "  -r, --ram VALUE      limitation of memory usage. E.g. '${C_LGn}512m${RES}' (default), '${C_LGn}1G${RES}'"
		echo -e "  -u, --uninstall      uninstall NUMBER nodes"
		echo
		echo -e "You can use either \"=\" or \" \" as an option and value ${C_LGn}delimiter${RES}"
		echo
		echo -e "${C_LGn}Useful URLs${RES}:"
		echo -e "https://t.me/letskynode — node Community"
		echo
		return 0 2>/dev/null; exit 0
		;;
	-n*|--number*)
		if ! grep -q "=" <<< "$1"; then shift; fi
		number=`option_value "$1"`
		shift
		;;
	-r*|--ram*)
		if ! grep -q "=" <<< "$1"; then shift; fi
		ram=`option_value "$1"`
		shift
		;;
	-u|--uninstall)
		uninstall="true"
		shift
		;;
	*)
		break
		;;
	esac
done
# Functions
printf_n(){ printf "$1\n" "${@:2}"; }
# Actions
sudo apt install wget -y &>/dev/null
. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/logo.sh)
if [ "$uninstall" = "true" ]; then
	for ((i=1; i < $number+1; i++)); do
		printf "Uninstalling ${C_LGn}${i}${RES} node out of ${C_LGn}${number}${RES}. Please enter the port: "
		read -r port
		wget -qO- "localhost:${port}/quit" >/dev/null
		sudo systemctl stop "$service"
		rm -rf "$HOME/minima/${port}" "/etc/systemd/system/minima_${port}.service"
		sudo systemctl daemon-reload
	done
	printf_n "
${C_LGn}Waiting 30 seconds to stop...${RES}"
	sleep 30
	printf_n "${C_LGn}Done!${RES}"
else
	sudo apt update
	sudo apt upgrade -y
	sudo apt install openjdk-11-jre-headless -y
	mkdir $HOME/minima/
	cd /etc/systemd/system/
	for service in minima_*; do
		wget -qO- "localhost:`cat $service | grep -oP "(?<=-port )([^%]+)(?= )"`/quit"
		sudo systemctl stop "$service"
	done
	cd
	wget -qO "$HOME/minima/minima.jar.new" https://github.com/minima-global/Minima/raw/master/jar/minima.jar
	mv $HOME/minima/minima.jar $HOME/minima/minima.jar.bk 2>/dev/null
	mv $HOME/minima/minima.jar.new $HOME/minima/minima.jar 2>/dev/null
	printf_n "
${C_LGn}Nodes installation...${RES}"
	urls=()
	ports_l=()
	ip=`wget -qO- eth0.me`
	for ((i=1; i < $number+1; i++)); do
		port=$((4991+10*$i))
		printf "Installing ${C_LGn}${i}${RES} node out of ${C_LGn}${number}${RES}. Please enter the port (default is ${C_LGn}${port}${RES}): "
		read -r port
		if [ -z "${port}" ]; then
			port=$((4991+10*$i))
		fi
		urls+=("${ip}:$((${port}+3))")
		ports_l+=("$port")
		dir="$HOME/minima/${port}"
		mkdir -p $dir
		sudo tee <<EOF >/dev/null "/etc/systemd/system/minima_${port}.service"
[Unit]
Description=Minima Node ${i}
After=network-online.target

[Service]
User=$USER
ExecStart=`which java` -Xmx${ram} -jar "$HOME/minima/minima.jar" -daemon -port ${port} -conf "$dir"
Restart=always
RestartSec=3
LimitNOFILE=65535

[Install]
WantedBy=multi-user.target
EOF
	done
	sudo systemctl daemon-reload
	cd /etc/systemd/system/
	for service in minima_*; do
		sudo systemctl enable "$service"
		sudo systemctl restart "$service"
	done
	cd
	printf_n "${C_LGn}Done!${RES}"
	. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/logo.sh)
	printf_n "
The nodes were ${C_LGn}started${RES}!

\tv ${C_LGn}Useful commands${RES} v

To view the node status:${C_LGn}"
	for port in ${ports_l[@]}; do
	  printf_n "systemctl status minima_${port}"
	done
	printf_n "${RES}
To view the node log:${C_LGn}"
	for port in ${ports_l[@]}; do
	  printf_n "sudo journalctl -f -n 100 -u minima_${port}"
	done
	printf_n "${RES}
To restart the node:${C_LGn}"
	for port in ${ports_l[@]}; do
	  printf_n "systemctl restart minima_${port}"
	done
	printf_n "${RES}
Web interface URL:${C_LGn}"
	for url in ${urls[@]}; do
	  printf_n "$url"
	done
	printf_n "${RES}"
fi
